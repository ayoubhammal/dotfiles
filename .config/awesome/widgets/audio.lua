local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local audio_widget = wibox.widget({
    {
        {
            id = "audio_text",
            widget = wibox.widget.textbox,
            markup = string.format("<span foreground='%s'>vol </span>", beautiful.color7),
        },
        {
            id = "audio_volume",
            widget = wibox.widget.textbox,
        },
        layout = wibox.layout.align.horizontal
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    volume_string_template = "<span foreground='%s'>%3d</span>",
    volume_fetch_command = "pamixer --get-volume",
    mute_fetch_command = "pamixer --get-mute",
})

function audio_widget:set_audio(volume, muted)
    local child_icon = self:get_children_by_id("audio_text")[1]
    local child_volume = self:get_children_by_id("audio_volume")[1]

    if volume ~= child_volume.volume or muted ~= child_icon.muted then
        local text_color =  muted and beautiful.color7 or beautiful.fg_normal
        child_icon.muted = muted
        child_volume.volume = volume
        child_volume:set_markup_silently(string.format(self.volume_string_template, text_color, volume))
    end
end

function audio_widget:check_audio()
    awful.spawn.easy_async(
        self.mute_fetch_command,
        function(stdout)
            local mute = stdout == "true\n"
            awful.spawn.easy_async(
                self.volume_fetch_command,
                function(stdout)
                    local volume = tonumber(stdout)
                    pcall(self.set_audio, self, volume, mute)
                end
            )
        end
    )
end

return audio_widget
