local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local caps_widget = wibox.widget({
    {
        id = "caps_indicator",
        widget = wibox.widget.textbox,
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    lock_text = string.format("<span foreground='%s'>CL</span>", beautiful.fg_normal),
    unlock_text = string.format("<span foreground='%s'>cl</span>", beautiful.color7),
    caps_fetch_command = "xset -q | grep Caps | awk '{ print $4 }'",
})

function caps_widget:set_lock(lock)
    local child = self:get_children_by_id("caps_indicator")[1]

    if child.lock ~= lock then
        child.lock = lock
    end

    if lock then
        child:set_markup_silently(self.lock_text)
    else
        child:set_markup_silently(self.unlock_text)
    end
end

function caps_widget:check_lock()
    awful.spawn.easy_async_with_shell(
        self.caps_fetch_command,
        function(stdout)
            pcall(self.set_lock, self, stdout == "on\n")
        end
    )
end

return caps_widget
