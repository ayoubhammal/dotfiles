local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local brightness_widget = wibox.widget({
    {
        {
            id = "brightness_text",
            widget = wibox.widget.textbox,
            markup = string.format("<span foreground='%s'>bri </span>", beautiful.color7),
        },
        {
            id = "brightness_level",
            widget = wibox.widget.textbox,
        },
        layout = wibox.layout.align.horizontal
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    brightness_string_template = "%3d",
    brightness_fetch_command = "brightnessctl | grep \"Current brightness\" | sed 's/^.\\+(\\([0-9]\\+\\)%)$/\\1/g'",
})

function brightness_widget:set_level(level)
    local child = self:get_children_by_id("brightness_level")[1]
    if child.level ~= level then
        child.level = level
        child:set_markup_silently(string.format(self.brightness_string_template, level))
    end
end

function brightness_widget:check_level()
    awful.spawn.easy_async_with_shell(
        self.brightness_fetch_command,
        function(stdout)
            pcall(self.set_level, self, tonumber(stdout))
        end
    )
end

return brightness_widget
