local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local mic_widget = wibox.widget({
    {
        id = "mic_text",
        widget = wibox.widget.textbox,
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    muted_text = string.format("<span foreground='%s'>mm</span>", beautiful.color7),
    unmuted_text = string.format("<span foreground='%s'>MU</span>", beautiful.fg_normal),
    mic_fetch_command = "pamixer --default-source --get-mute",
})

function mic_widget:set_mic(muted)
    local child = self:get_children_by_id("mic_text")[1]

    if muted ~= child.muted then
        child.muted = muted
        if muted then
            child:set_markup_silently(self.muted_text)
        else
            child:set_markup_silently(self.unmuted_text)
        end
    end
end

function mic_widget:check_mic()
    awful.spawn.easy_async(
        self.mic_fetch_command,
        function(stdout)
            pcall(self.set_mic, self, stdout == "true\n")
        end
    )
end

return mic_widget
