local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
local gears = require("gears")
local naughty = require("naughty")
beautiful.init("~/.config/awesome/theme.lua")


function Calculate_next_date(day, month, year)
    local days_per_month = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
    -- check if the year is a leap year
    if year % 4 == 0 and year % 100 ~= 0 or year % 400 == 0 then
        days_per_month[2] = 29
    end

    local next = {}

    next.day = (day % days_per_month[month]) + 1
    if days_per_month == day then
        next.month = (month % 12) + 1
        if month == 12 then
            next.year = year + 1
        else
            next.year = year
        end
    else
        next.month = month
        next.year = year
    end

    return next
end

function Calculate_previous_date(day, month, year)
    local days_per_month = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 }
    -- check if the year is a leap year
    if year % 4 == 0 and year % 100 ~= 0 or year % 400 == 0 then
        days_per_month[2] = 29
    end

    local previous = {}

    if day == 1 then
        if month == 1 then
            previous.month = 12
            previous.year = year - 1
        else
            previous.month = month - 1
            previous.year = year
        end
        previous.day = days_per_month[previous.month]
    else
        previous.year = year
        previous.month = month
        previous.day = day - 1
    end

    return previous
end


local prayer_widget = wibox.widget({
    {
        id = "prayer",
        widget = wibox.widget.textbox,
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    masjid = "Gentilly",
    times = require("widgets.assets.gentilly"),
    prayers = {
        "fajr",
        "douha",
        "dhuhr",
        "asr",
        "maghrib",
        "isha"
    },
    current_prayer = nil,
    prayer_string_template = "%s  %02d:%02d ▹ %02d:%02d",
})

function prayer_widget:set_prayer(prayer)
    local child = self:get_children_by_id("prayer")[1]
    if self.current_prayer ~= prayer then
        self.current_prayer = prayer
        child:set_markup_silently(string.format("<span foreground='%s'><i>%s</i></span>", beautiful.color7, prayer))
    end
end

function prayer_widget:check_prayer()
    local current_prayer = nil
    local prev_hour = nil
    local prev_min = nil
    local next_hour = nil
    local next_min = nil

    local current_datetime = os.date("*t")
    local prayer_table = self.times[current_datetime.month][current_datetime.day]

    if current_datetime.hour < prayer_table[1].hour or (current_datetime.hour == prayer_table[1].hour and current_datetime.min < prayer_table[1].min) then
        -- the case where we are before the first prayer of the day
        current_prayer = self.prayers[#self.prayers]
        next_hour = prayer_table[1].hour
        next_min = prayer_table[1].min

        local prev_day_datetime = Calculate_previous_date(current_datetime.day, current_datetime.month, current_datetime.year)
        prev_hour = self.times[prev_day_datetime.month][prev_day_datetime.day][6].hour
        prev_min = self.times[prev_day_datetime.month][prev_day_datetime.day][6].min
    else
        local current_prayer_index = nil

        for i = 1, #self.prayers do
            local prayer_time = prayer_table[i]
            if (prayer_time.hour < current_datetime.hour) or (prayer_time.hour == current_datetime.hour and prayer_time.min < current_datetime.min) then
                current_prayer_index = i
            else
                break
            end
        end

        if current_prayer_index == #self.prayers then
            local next_day_datetime = Calculate_next_date(current_datetime.day, current_datetime.month, current_datetime.year)

            current_prayer = self.prayers[current_prayer_index]
            next_hour = self.times[next_day_datetime.month][next_day_datetime.day][1].hour
            next_min = self.times[next_day_datetime.month][next_day_datetime.day][1].min
        else
            current_prayer = self.prayers[current_prayer_index]
            next_hour = prayer_table[current_prayer_index + 1].hour
            next_min = prayer_table[current_prayer_index + 1].min
        end
        prev_hour = prayer_table[current_prayer_index].hour
        prev_min = prayer_table[current_prayer_index].min

    end
    self:set_prayer(
        string.format(
            self.prayer_string_template,
            current_prayer,
            prev_hour,
            prev_min,
            next_hour,
            next_min
        )
    )

end

prayer_widget:buttons(gears.table.join(
    awful.button(
        { },
        1,
        function()
            local current_datetime = os.date("*t")
            local prayer_table = prayer_widget.times[current_datetime.month][current_datetime.day]
            local schedule = {}
            for k, time in pairs(prayer_table) do
                schedule[#schedule + 1] = string.format("%10s  %02d:%02d", prayer_widget.prayers[k], time.hour, time.min)
            end

            naughty.notify({
                text =  table.concat(schedule, "\n"),
                title = string.format("Schedule %s\nMosque %s", os.date("%Y-%m-%d"), prayer_widget.masjid)
            })
        end
    )
))

return prayer_widget
