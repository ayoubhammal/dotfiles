local times = {
    {
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 48
			},
            {
				hour = 17,
				min = 8
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 49
			},
            {
				hour = 17,
				min = 9
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 50
			},
            {
				hour = 17,
				min = 10
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 1
			},
            {
				hour = 14,
				min = 51
			},
            {
				hour = 17,
				min = 11
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 1
			},
            {
				hour = 14,
				min = 52
			},
            {
				hour = 17,
				min = 12
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 1
			},
            {
				hour = 14,
				min = 52
			},
            {
				hour = 17,
				min = 13
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 2
			},
            {
				hour = 14,
				min = 53
			},
            {
				hour = 17,
				min = 14
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 2
			},
            {
				hour = 14,
				min = 54
			},
            {
				hour = 17,
				min = 15
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 42
			},
            {
				hour = 13,
				min = 3
			},
            {
				hour = 14,
				min = 55
			},
            {
				hour = 17,
				min = 17
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 42
			},
            {
				hour = 13,
				min = 3
			},
            {
				hour = 14,
				min = 56
			},
            {
				hour = 17,
				min = 18
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 41
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 14,
				min = 57
			},
            {
				hour = 17,
				min = 19
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 19
			},
            {
				hour = 8,
				min = 41
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 14,
				min = 58
			},
            {
				hour = 17,
				min = 21
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 19
			},
            {
				hour = 8,
				min = 40
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 15,
				min = 0
			},
            {
				hour = 17,
				min = 22
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 18
			},
            {
				hour = 8,
				min = 40
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 15,
				min = 1
			},
            {
				hour = 17,
				min = 23
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 18
			},
            {
				hour = 8,
				min = 39
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 15,
				min = 2
			},
            {
				hour = 17,
				min = 25
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 17
			},
            {
				hour = 8,
				min = 38
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 15,
				min = 3
			},
            {
				hour = 17,
				min = 26
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 17
			},
            {
				hour = 8,
				min = 38
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 15,
				min = 4
			},
            {
				hour = 17,
				min = 28
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 16
			},
            {
				hour = 8,
				min = 37
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 15,
				min = 5
			},
            {
				hour = 17,
				min = 29
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 15
			},
            {
				hour = 8,
				min = 36
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 15,
				min = 7
			},
            {
				hour = 17,
				min = 30
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 15
			},
            {
				hour = 8,
				min = 35
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 15,
				min = 8
			},
            {
				hour = 17,
				min = 32
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 14
			},
            {
				hour = 8,
				min = 34
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 15,
				min = 9
			},
            {
				hour = 17,
				min = 34
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 13
			},
            {
				hour = 8,
				min = 33
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 15,
				min = 10
			},
            {
				hour = 17,
				min = 35
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 12
			},
            {
				hour = 8,
				min = 32
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 11
			},
            {
				hour = 17,
				min = 37
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 12
			},
            {
				hour = 8,
				min = 31
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 13
			},
            {
				hour = 17,
				min = 38
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 11
			},
            {
				hour = 8,
				min = 30
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 14
			},
            {
				hour = 17,
				min = 40
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 10
			},
            {
				hour = 8,
				min = 29
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 15
			},
            {
				hour = 17,
				min = 41
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 9
			},
            {
				hour = 8,
				min = 28
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 17
			},
            {
				hour = 17,
				min = 43
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 8
			},
            {
				hour = 8,
				min = 26
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 18
			},
            {
				hour = 17,
				min = 45
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 7
			},
            {
				hour = 8,
				min = 25
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 19
			},
            {
				hour = 17,
				min = 46
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 6
			},
            {
				hour = 8,
				min = 24
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 20
			},
            {
				hour = 17,
				min = 48
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 5
			},
            {
				hour = 8,
				min = 23
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 22
			},
            {
				hour = 17,
				min = 49
			},
            {
				hour = 19,
				min = 50
			}
        }
    },
    {
        {
            {
				hour = 7,
				min = 3
			},
            {
				hour = 8,
				min = 21
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 23
			},
            {
				hour = 17,
				min = 51
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 2
			},
            {
				hour = 8,
				min = 20
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 24
			},
            {
				hour = 17,
				min = 53
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 1
			},
            {
				hour = 8,
				min = 19
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 26
			},
            {
				hour = 17,
				min = 54
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 0
			},
            {
				hour = 8,
				min = 17
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 27
			},
            {
				hour = 17,
				min = 56
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 59
			},
            {
				hour = 8,
				min = 16
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 28
			},
            {
				hour = 17,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 57
			},
            {
				hour = 8,
				min = 14
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 30
			},
            {
				hour = 17,
				min = 59
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 56
			},
            {
				hour = 8,
				min = 13
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 31
			},
            {
				hour = 18,
				min = 1
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 54
			},
            {
				hour = 8,
				min = 11
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 32
			},
            {
				hour = 18,
				min = 3
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 53
			},
            {
				hour = 8,
				min = 9
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 34
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 52
			},
            {
				hour = 8,
				min = 8
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 35
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 50
			},
            {
				hour = 8,
				min = 6
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 36
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 49
			},
            {
				hour = 8,
				min = 5
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 38
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 47
			},
            {
				hour = 8,
				min = 3
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 39
			},
            {
				hour = 18,
				min = 11
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 46
			},
            {
				hour = 8,
				min = 1
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 40
			},
            {
				hour = 18,
				min = 12
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 44
			},
            {
				hour = 7,
				min = 59
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 41
			},
            {
				hour = 18,
				min = 14
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 42
			},
            {
				hour = 7,
				min = 58
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 43
			},
            {
				hour = 18,
				min = 16
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 41
			},
            {
				hour = 7,
				min = 56
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 44
			},
            {
				hour = 18,
				min = 17
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 39
			},
            {
				hour = 7,
				min = 54
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 45
			},
            {
				hour = 18,
				min = 19
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 37
			},
            {
				hour = 7,
				min = 52
			},
            {
				hour = 13,
				min = 10
			},
            {
				hour = 15,
				min = 46
			},
            {
				hour = 18,
				min = 21
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 36
			},
            {
				hour = 7,
				min = 51
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 48
			},
            {
				hour = 18,
				min = 22
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 34
			},
            {
				hour = 7,
				min = 49
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 49
			},
            {
				hour = 18,
				min = 24
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 32
			},
            {
				hour = 7,
				min = 47
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 50
			},
            {
				hour = 18,
				min = 25
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 30
			},
            {
				hour = 7,
				min = 45
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 51
			},
            {
				hour = 18,
				min = 27
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 29
			},
            {
				hour = 7,
				min = 43
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 53
			},
            {
				hour = 18,
				min = 29
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 27
			},
            {
				hour = 7,
				min = 41
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 54
			},
            {
				hour = 18,
				min = 30
			},
            {
				hour = 19,
				min = 52
			}
        },
        {
            {
				hour = 6,
				min = 25
			},
            {
				hour = 7,
				min = 39
			},
            {
				hour = 13,
				min = 9
			},
            {
				hour = 15,
				min = 55
			},
            {
				hour = 18,
				min = 32
			},
            {
				hour = 19,
				min = 53
			}
        },
        {
            {
				hour = 6,
				min = 23
			},
            {
				hour = 7,
				min = 37
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 56
			},
            {
				hour = 18,
				min = 34
			},
            {
				hour = 19,
				min = 55
			}
        },
        {
            {
				hour = 6,
				min = 21
			},
            {
				hour = 7,
				min = 35
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 57
			},
            {
				hour = 18,
				min = 35
			},
            {
				hour = 19,
				min = 56
			}
        },
        {
            {
				hour = 6,
				min = 19
			},
            {
				hour = 7,
				min = 33
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 58
			},
            {
				hour = 18,
				min = 37
			},
            {
				hour = 19,
				min = 58
			}
        }
    },
    {
        {
            {
				hour = 6,
				min = 19
			},
            {
				hour = 7,
				min = 33
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 58
			},
            {
				hour = 18,
				min = 37
			},
            {
				hour = 19,
				min = 58
			}
        },
        {
            {
				hour = 6,
				min = 17
			},
            {
				hour = 7,
				min = 31
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 15,
				min = 59
			},
            {
				hour = 18,
				min = 38
			},
            {
				hour = 19,
				min = 59
			}
        },
        {
            {
				hour = 6,
				min = 15
			},
            {
				hour = 7,
				min = 29
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 16,
				min = 1
			},
            {
				hour = 18,
				min = 40
			},
            {
				hour = 20,
				min = 1
			}
        },
        {
            {
				hour = 6,
				min = 14
			},
            {
				hour = 7,
				min = 27
			},
            {
				hour = 13,
				min = 8
			},
            {
				hour = 16,
				min = 2
			},
            {
				hour = 18,
				min = 41
			},
            {
				hour = 20,
				min = 2
			}
        },
        {
            {
				hour = 6,
				min = 12
			},
            {
				hour = 7,
				min = 25
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 16,
				min = 3
			},
            {
				hour = 18,
				min = 43
			},
            {
				hour = 20,
				min = 4
			}
        },
        {
            {
				hour = 6,
				min = 10
			},
            {
				hour = 7,
				min = 23
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 16,
				min = 4
			},
            {
				hour = 18,
				min = 45
			},
            {
				hour = 20,
				min = 6
			}
        },
        {
            {
				hour = 6,
				min = 8
			},
            {
				hour = 7,
				min = 21
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 16,
				min = 5
			},
            {
				hour = 18,
				min = 46
			},
            {
				hour = 20,
				min = 7
			}
        },
        {
            {
				hour = 6,
				min = 6
			},
            {
				hour = 7,
				min = 19
			},
            {
				hour = 13,
				min = 7
			},
            {
				hour = 16,
				min = 6
			},
            {
				hour = 18,
				min = 48
			},
            {
				hour = 20,
				min = 9
			}
        },
        {
            {
				hour = 6,
				min = 3
			},
            {
				hour = 7,
				min = 17
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 16,
				min = 7
			},
            {
				hour = 18,
				min = 49
			},
            {
				hour = 20,
				min = 10
			}
        },
        {
            {
				hour = 6,
				min = 1
			},
            {
				hour = 7,
				min = 15
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 16,
				min = 8
			},
            {
				hour = 18,
				min = 51
			},
            {
				hour = 20,
				min = 12
			}
        },
        {
            {
				hour = 5,
				min = 59
			},
            {
				hour = 7,
				min = 13
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 16,
				min = 9
			},
            {
				hour = 18,
				min = 52
			},
            {
				hour = 20,
				min = 13
			}
        },
        {
            {
				hour = 5,
				min = 57
			},
            {
				hour = 7,
				min = 11
			},
            {
				hour = 13,
				min = 6
			},
            {
				hour = 16,
				min = 10
			},
            {
				hour = 18,
				min = 54
			},
            {
				hour = 20,
				min = 15
			}
        },
        {
            {
				hour = 5,
				min = 55
			},
            {
				hour = 7,
				min = 9
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 16,
				min = 11
			},
            {
				hour = 18,
				min = 55
			},
            {
				hour = 20,
				min = 16
			}
        },
        {
            {
				hour = 5,
				min = 53
			},
            {
				hour = 7,
				min = 7
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 16,
				min = 12
			},
            {
				hour = 18,
				min = 57
			},
            {
				hour = 20,
				min = 18
			}
        },
        {
            {
				hour = 5,
				min = 51
			},
            {
				hour = 7,
				min = 5
			},
            {
				hour = 13,
				min = 5
			},
            {
				hour = 16,
				min = 13
			},
            {
				hour = 18,
				min = 58
			},
            {
				hour = 20,
				min = 20
			}
        },
        {
            {
				hour = 5,
				min = 49
			},
            {
				hour = 7,
				min = 3
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 16,
				min = 14
			},
            {
				hour = 19,
				min = 0
			},
            {
				hour = 20,
				min = 21
			}
        },
        {
            {
				hour = 5,
				min = 47
			},
            {
				hour = 7,
				min = 1
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 16,
				min = 15
			},
            {
				hour = 19,
				min = 1
			},
            {
				hour = 20,
				min = 23
			}
        },
        {
            {
				hour = 5,
				min = 44
			},
            {
				hour = 6,
				min = 59
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 16,
				min = 16
			},
            {
				hour = 19,
				min = 3
			},
            {
				hour = 20,
				min = 24
			}
        },
        {
            {
				hour = 5,
				min = 42
			},
            {
				hour = 6,
				min = 57
			},
            {
				hour = 13,
				min = 4
			},
            {
				hour = 16,
				min = 17
			},
            {
				hour = 19,
				min = 5
			},
            {
				hour = 20,
				min = 26
			}
        },
        {
            {
				hour = 5,
				min = 40
			},
            {
				hour = 6,
				min = 54
			},
            {
				hour = 13,
				min = 3
			},
            {
				hour = 16,
				min = 18
			},
            {
				hour = 19,
				min = 6
			},
            {
				hour = 20,
				min = 28
			}
        },
        {
            {
				hour = 5,
				min = 38
			},
            {
				hour = 6,
				min = 52
			},
            {
				hour = 13,
				min = 3
			},
            {
				hour = 16,
				min = 19
			},
            {
				hour = 19,
				min = 8
			},
            {
				hour = 20,
				min = 29
			}
        },
        {
            {
				hour = 5,
				min = 36
			},
            {
				hour = 6,
				min = 50
			},
            {
				hour = 13,
				min = 3
			},
            {
				hour = 16,
				min = 20
			},
            {
				hour = 19,
				min = 9
			},
            {
				hour = 20,
				min = 31
			}
        },
        {
            {
				hour = 5,
				min = 33
			},
            {
				hour = 6,
				min = 48
			},
            {
				hour = 13,
				min = 2
			},
            {
				hour = 16,
				min = 20
			},
            {
				hour = 19,
				min = 11
			},
            {
				hour = 20,
				min = 32
			}
        },
        {
            {
				hour = 5,
				min = 31
			},
            {
				hour = 6,
				min = 46
			},
            {
				hour = 13,
				min = 2
			},
            {
				hour = 16,
				min = 21
			},
            {
				hour = 19,
				min = 12
			},
            {
				hour = 20,
				min = 34
			}
        },
        {
            {
				hour = 5,
				min = 29
			},
            {
				hour = 6,
				min = 44
			},
            {
				hour = 13,
				min = 2
			},
            {
				hour = 16,
				min = 22
			},
            {
				hour = 19,
				min = 14
			},
            {
				hour = 20,
				min = 36
			}
        },
        {
            {
				hour = 6,
				min = 27
			},
            {
				hour = 7,
				min = 42
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 23
			},
            {
				hour = 20,
				min = 15
			},
            {
				hour = 21,
				min = 37
			}
        },
        {
            {
				hour = 6,
				min = 24
			},
            {
				hour = 7,
				min = 40
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 24
			},
            {
				hour = 20,
				min = 17
			},
            {
				hour = 21,
				min = 39
			}
        },
        {
            {
				hour = 6,
				min = 22
			},
            {
				hour = 7,
				min = 38
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 25
			},
            {
				hour = 20,
				min = 18
			},
            {
				hour = 21,
				min = 41
			}
        },
        {
            {
				hour = 6,
				min = 20
			},
            {
				hour = 7,
				min = 36
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 25
			},
            {
				hour = 20,
				min = 20
			},
            {
				hour = 21,
				min = 42
			}
        },
        {
            {
				hour = 6,
				min = 18
			},
            {
				hour = 7,
				min = 33
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 26
			},
            {
				hour = 20,
				min = 21
			},
            {
				hour = 21,
				min = 44
			}
        },
        {
            {
				hour = 6,
				min = 15
			},
            {
				hour = 7,
				min = 31
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 27
			},
            {
				hour = 20,
				min = 23
			},
            {
				hour = 21,
				min = 46
			}
        }
    },
    {
        {
            {
				hour = 6,
				min = 13
			},
            {
				hour = 7,
				min = 29
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 28
			},
            {
				hour = 20,
				min = 24
			},
            {
				hour = 21,
				min = 47
			}
        },
        {
            {
				hour = 6,
				min = 11
			},
            {
				hour = 7,
				min = 27
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 29
			},
            {
				hour = 20,
				min = 26
			},
            {
				hour = 21,
				min = 49
			}
        },
        {
            {
				hour = 6,
				min = 9
			},
            {
				hour = 7,
				min = 25
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 29
			},
            {
				hour = 20,
				min = 27
			},
            {
				hour = 21,
				min = 51
			}
        },
        {
            {
				hour = 6,
				min = 6
			},
            {
				hour = 7,
				min = 23
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 30
			},
            {
				hour = 20,
				min = 29
			},
            {
				hour = 21,
				min = 52
			}
        },
        {
            {
				hour = 6,
				min = 4
			},
            {
				hour = 7,
				min = 21
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 31
			},
            {
				hour = 20,
				min = 30
			},
            {
				hour = 21,
				min = 54
			}
        },
        {
            {
				hour = 6,
				min = 2
			},
            {
				hour = 7,
				min = 19
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 32
			},
            {
				hour = 20,
				min = 31
			},
            {
				hour = 21,
				min = 56
			}
        },
        {
            {
				hour = 5,
				min = 59
			},
            {
				hour = 7,
				min = 17
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 32
			},
            {
				hour = 20,
				min = 33
			},
            {
				hour = 21,
				min = 58
			}
        },
        {
            {
				hour = 5,
				min = 57
			},
            {
				hour = 7,
				min = 15
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 33
			},
            {
				hour = 20,
				min = 34
			},
            {
				hour = 21,
				min = 59
			}
        },
        {
            {
				hour = 5,
				min = 55
			},
            {
				hour = 7,
				min = 13
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 34
			},
            {
				hour = 20,
				min = 36
			},
            {
				hour = 22,
				min = 1
			}
        },
        {
            {
				hour = 5,
				min = 52
			},
            {
				hour = 7,
				min = 11
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 35
			},
            {
				hour = 20,
				min = 37
			},
            {
				hour = 22,
				min = 3
			}
        },
        {
            {
				hour = 5,
				min = 50
			},
            {
				hour = 7,
				min = 9
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 35
			},
            {
				hour = 20,
				min = 39
			},
            {
				hour = 22,
				min = 5
			}
        },
        {
            {
				hour = 5,
				min = 48
			},
            {
				hour = 7,
				min = 7
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 36
			},
            {
				hour = 20,
				min = 40
			},
            {
				hour = 22,
				min = 6
			}
        },
        {
            {
				hour = 5,
				min = 46
			},
            {
				hour = 7,
				min = 5
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 37
			},
            {
				hour = 20,
				min = 42
			},
            {
				hour = 22,
				min = 8
			}
        },
        {
            {
				hour = 5,
				min = 43
			},
            {
				hour = 7,
				min = 3
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 37
			},
            {
				hour = 20,
				min = 43
			},
            {
				hour = 22,
				min = 10
			}
        },
        {
            {
				hour = 5,
				min = 41
			},
            {
				hour = 7,
				min = 1
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 38
			},
            {
				hour = 20,
				min = 45
			},
            {
				hour = 22,
				min = 12
			}
        },
        {
            {
				hour = 5,
				min = 39
			},
            {
				hour = 6,
				min = 59
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 39
			},
            {
				hour = 20,
				min = 46
			},
            {
				hour = 22,
				min = 14
			}
        },
        {
            {
				hour = 5,
				min = 36
			},
            {
				hour = 6,
				min = 57
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 39
			},
            {
				hour = 20,
				min = 48
			},
            {
				hour = 22,
				min = 16
			}
        },
        {
            {
				hour = 5,
				min = 34
			},
            {
				hour = 6,
				min = 55
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 40
			},
            {
				hour = 20,
				min = 49
			},
            {
				hour = 22,
				min = 17
			}
        },
        {
            {
				hour = 5,
				min = 32
			},
            {
				hour = 6,
				min = 53
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 41
			},
            {
				hour = 20,
				min = 51
			},
            {
				hour = 22,
				min = 19
			}
        },
        {
            {
				hour = 5,
				min = 29
			},
            {
				hour = 6,
				min = 51
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 41
			},
            {
				hour = 20,
				min = 52
			},
            {
				hour = 22,
				min = 21
			}
        },
        {
            {
				hour = 5,
				min = 27
			},
            {
				hour = 6,
				min = 49
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 42
			},
            {
				hour = 20,
				min = 54
			},
            {
				hour = 22,
				min = 23
			}
        },
        {
            {
				hour = 5,
				min = 25
			},
            {
				hour = 6,
				min = 47
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 42
			},
            {
				hour = 20,
				min = 55
			},
            {
				hour = 22,
				min = 25
			}
        },
        {
            {
				hour = 5,
				min = 23
			},
            {
				hour = 6,
				min = 45
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 43
			},
            {
				hour = 20,
				min = 57
			},
            {
				hour = 22,
				min = 27
			}
        },
        {
            {
				hour = 5,
				min = 20
			},
            {
				hour = 6,
				min = 43
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 44
			},
            {
				hour = 20,
				min = 58
			},
            {
				hour = 22,
				min = 29
			}
        },
        {
            {
				hour = 5,
				min = 18
			},
            {
				hour = 6,
				min = 42
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 44
			},
            {
				hour = 21,
				min = 0
			},
            {
				hour = 22,
				min = 31
			}
        },
        {
            {
				hour = 5,
				min = 16
			},
            {
				hour = 6,
				min = 40
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 45
			},
            {
				hour = 21,
				min = 1
			},
            {
				hour = 22,
				min = 32
			}
        },
        {
            {
				hour = 5,
				min = 14
			},
            {
				hour = 6,
				min = 38
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 45
			},
            {
				hour = 21,
				min = 3
			},
            {
				hour = 22,
				min = 34
			}
        },
        {
            {
				hour = 5,
				min = 11
			},
            {
				hour = 6,
				min = 36
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 46
			},
            {
				hour = 21,
				min = 4
			},
            {
				hour = 22,
				min = 36
			}
        },
        {
            {
				hour = 5,
				min = 9
			},
            {
				hour = 6,
				min = 34
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 47
			},
            {
				hour = 21,
				min = 6
			},
            {
				hour = 22,
				min = 38
			}
        },
        {
            {
				hour = 5,
				min = 7
			},
            {
				hour = 6,
				min = 33
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 47
			},
            {
				hour = 21,
				min = 7
			},
            {
				hour = 22,
				min = 40
			}
        }
    },
    {
        {
            {
				hour = 5,
				min = 5
			},
            {
				hour = 6,
				min = 31
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 48
			},
            {
				hour = 21,
				min = 9
			},
            {
				hour = 22,
				min = 42
			}
        },
        {
            {
				hour = 5,
				min = 3
			},
            {
				hour = 6,
				min = 29
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 48
			},
            {
				hour = 21,
				min = 10
			},
            {
				hour = 22,
				min = 44
			}
        },
        {
            {
				hour = 5,
				min = 1
			},
            {
				hour = 6,
				min = 28
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 49
			},
            {
				hour = 21,
				min = 11
			},
            {
				hour = 22,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 58
			},
            {
				hour = 6,
				min = 26
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 49
			},
            {
				hour = 21,
				min = 13
			},
            {
				hour = 22,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 56
			},
            {
				hour = 6,
				min = 24
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 50
			},
            {
				hour = 21,
				min = 14
			},
            {
				hour = 22,
				min = 50
			}
        },
        {
            {
				hour = 4,
				min = 54
			},
            {
				hour = 6,
				min = 23
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 50
			},
            {
				hour = 21,
				min = 16
			},
            {
				hour = 22,
				min = 52
			}
        },
        {
            {
				hour = 4,
				min = 52
			},
            {
				hour = 6,
				min = 21
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 51
			},
            {
				hour = 21,
				min = 17
			},
            {
				hour = 22,
				min = 54
			}
        },
        {
            {
				hour = 4,
				min = 50
			},
            {
				hour = 6,
				min = 20
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 52
			},
            {
				hour = 21,
				min = 19
			},
            {
				hour = 22,
				min = 56
			}
        },
        {
            {
				hour = 4,
				min = 48
			},
            {
				hour = 6,
				min = 18
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 52
			},
            {
				hour = 21,
				min = 20
			},
            {
				hour = 22,
				min = 58
			}
        },
        {
            {
				hour = 4,
				min = 46
			},
            {
				hour = 6,
				min = 17
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 53
			},
            {
				hour = 21,
				min = 21
			},
            {
				hour = 23,
				min = 0
			}
        },
        {
            {
				hour = 4,
				min = 44
			},
            {
				hour = 6,
				min = 15
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 53
			},
            {
				hour = 21,
				min = 23
			},
            {
				hour = 23,
				min = 2
			}
        },
        {
            {
				hour = 4,
				min = 42
			},
            {
				hour = 6,
				min = 14
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 54
			},
            {
				hour = 21,
				min = 24
			},
            {
				hour = 23,
				min = 3
			}
        },
        {
            {
				hour = 4,
				min = 40
			},
            {
				hour = 6,
				min = 12
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 54
			},
            {
				hour = 21,
				min = 26
			},
            {
				hour = 23,
				min = 5
			}
        },
        {
            {
				hour = 4,
				min = 38
			},
            {
				hour = 6,
				min = 11
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 55
			},
            {
				hour = 21,
				min = 27
			},
            {
				hour = 23,
				min = 7
			}
        },
        {
            {
				hour = 4,
				min = 36
			},
            {
				hour = 6,
				min = 10
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 55
			},
            {
				hour = 21,
				min = 28
			},
            {
				hour = 23,
				min = 9
			}
        },
        {
            {
				hour = 4,
				min = 34
			},
            {
				hour = 6,
				min = 8
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 56
			},
            {
				hour = 21,
				min = 30
			},
            {
				hour = 23,
				min = 11
			}
        },
        {
            {
				hour = 4,
				min = 32
			},
            {
				hour = 6,
				min = 7
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 56
			},
            {
				hour = 21,
				min = 31
			},
            {
				hour = 23,
				min = 13
			}
        },
        {
            {
				hour = 4,
				min = 30
			},
            {
				hour = 6,
				min = 6
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 57
			},
            {
				hour = 21,
				min = 32
			},
            {
				hour = 23,
				min = 15
			}
        },
        {
            {
				hour = 4,
				min = 29
			},
            {
				hour = 6,
				min = 5
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 57
			},
            {
				hour = 21,
				min = 33
			},
            {
				hour = 23,
				min = 17
			}
        },
        {
            {
				hour = 4,
				min = 27
			},
            {
				hour = 6,
				min = 3
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 58
			},
            {
				hour = 21,
				min = 35
			},
            {
				hour = 23,
				min = 19
			}
        },
        {
            {
				hour = 4,
				min = 25
			},
            {
				hour = 6,
				min = 2
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 58
			},
            {
				hour = 21,
				min = 36
			},
            {
				hour = 23,
				min = 20
			}
        },
        {
            {
				hour = 4,
				min = 23
			},
            {
				hour = 6,
				min = 1
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 58
			},
            {
				hour = 21,
				min = 37
			},
            {
				hour = 23,
				min = 22
			}
        },
        {
            {
				hour = 4,
				min = 22
			},
            {
				hour = 6,
				min = 0
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 59
			},
            {
				hour = 21,
				min = 38
			},
            {
				hour = 23,
				min = 24
			}
        },
        {
            {
				hour = 4,
				min = 20
			},
            {
				hour = 5,
				min = 59
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 59
			},
            {
				hour = 21,
				min = 40
			},
            {
				hour = 23,
				min = 26
			}
        },
        {
            {
				hour = 4,
				min = 19
			},
            {
				hour = 5,
				min = 58
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 0
			},
            {
				hour = 21,
				min = 41
			},
            {
				hour = 23,
				min = 28
			}
        },
        {
            {
				hour = 4,
				min = 17
			},
            {
				hour = 5,
				min = 57
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 0
			},
            {
				hour = 21,
				min = 42
			},
            {
				hour = 23,
				min = 29
			}
        },
        {
            {
				hour = 4,
				min = 16
			},
            {
				hour = 5,
				min = 56
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 1
			},
            {
				hour = 21,
				min = 43
			},
            {
				hour = 23,
				min = 31
			}
        },
        {
            {
				hour = 4,
				min = 14
			},
            {
				hour = 5,
				min = 55
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 1
			},
            {
				hour = 21,
				min = 44
			},
            {
				hour = 23,
				min = 33
			}
        },
        {
            {
				hour = 4,
				min = 13
			},
            {
				hour = 5,
				min = 54
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 2
			},
            {
				hour = 21,
				min = 45
			},
            {
				hour = 23,
				min = 34
			}
        },
        {
            {
				hour = 4,
				min = 12
			},
            {
				hour = 5,
				min = 54
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 2
			},
            {
				hour = 21,
				min = 46
			},
            {
				hour = 23,
				min = 36
			}
        },
        {
            {
				hour = 4,
				min = 10
			},
            {
				hour = 5,
				min = 53
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 18,
				min = 2
			},
            {
				hour = 21,
				min = 47
			},
            {
				hour = 23,
				min = 37
			}
        }
    },
    {
        {
            {
				hour = 4,
				min = 9
			},
            {
				hour = 5,
				min = 52
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 3
			},
            {
				hour = 21,
				min = 48
			},
            {
				hour = 23,
				min = 39
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 52
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 3
			},
            {
				hour = 21,
				min = 49
			},
            {
				hour = 23,
				min = 39
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 51
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 21,
				min = 50
			},
            {
				hour = 23,
				min = 40
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 50
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 21,
				min = 51
			},
            {
				hour = 23,
				min = 41
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 50
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 21,
				min = 52
			},
            {
				hour = 23,
				min = 41
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 49
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 18,
				min = 5
			},
            {
				hour = 21,
				min = 53
			},
            {
				hour = 23,
				min = 42
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 49
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 18,
				min = 5
			},
            {
				hour = 21,
				min = 54
			},
            {
				hour = 23,
				min = 42
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 18,
				min = 5
			},
            {
				hour = 21,
				min = 54
			},
            {
				hour = 23,
				min = 43
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 21,
				min = 55
			},
            {
				hour = 23,
				min = 43
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 21,
				min = 56
			},
            {
				hour = 23,
				min = 44
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 21,
				min = 56
			},
            {
				hour = 23,
				min = 44
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 57
			},
            {
				hour = 23,
				min = 45
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 58
			},
            {
				hour = 23,
				min = 45
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 58
			},
            {
				hour = 23,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 21,
				min = 59
			},
            {
				hour = 23,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 21,
				min = 59
			},
            {
				hour = 23,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 7
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 47
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 8
			},
            {
				hour = 5,
				min = 48
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 9
			},
            {
				hour = 5,
				min = 49
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 9
			},
            {
				hour = 5,
				min = 49
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 9
			},
            {
				hour = 5,
				min = 50
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 10
			},
            {
				hour = 5,
				min = 50
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 10
			},
            {
				hour = 5,
				min = 51
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        }
    },
    {
        {
            {
				hour = 4,
				min = 11
			},
            {
				hour = 5,
				min = 51
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 1
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 11
			},
            {
				hour = 5,
				min = 52
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 12
			},
            {
				hour = 5,
				min = 53
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 12
			},
            {
				hour = 5,
				min = 53
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 11
			},
            {
				hour = 22,
				min = 0
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 13
			},
            {
				hour = 5,
				min = 54
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 59
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 13
			},
            {
				hour = 5,
				min = 55
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 59
			},
            {
				hour = 23,
				min = 48
			}
        },
        {
            {
				hour = 4,
				min = 14
			},
            {
				hour = 5,
				min = 56
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 58
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 14
			},
            {
				hour = 5,
				min = 57
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 58
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 15
			},
            {
				hour = 5,
				min = 57
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 57
			},
            {
				hour = 23,
				min = 47
			}
        },
        {
            {
				hour = 4,
				min = 15
			},
            {
				hour = 5,
				min = 58
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 57
			},
            {
				hour = 23,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 16
			},
            {
				hour = 5,
				min = 59
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 56
			},
            {
				hour = 23,
				min = 46
			}
        },
        {
            {
				hour = 4,
				min = 17
			},
            {
				hour = 6,
				min = 0
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 55
			},
            {
				hour = 23,
				min = 45
			}
        },
        {
            {
				hour = 4,
				min = 18
			},
            {
				hour = 6,
				min = 1
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 54
			},
            {
				hour = 23,
				min = 44
			}
        },
        {
            {
				hour = 4,
				min = 20
			},
            {
				hour = 6,
				min = 2
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 10
			},
            {
				hour = 21,
				min = 54
			},
            {
				hour = 23,
				min = 42
			}
        },
        {
            {
				hour = 4,
				min = 22
			},
            {
				hour = 6,
				min = 3
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 21,
				min = 53
			},
            {
				hour = 23,
				min = 41
			}
        },
        {
            {
				hour = 4,
				min = 23
			},
            {
				hour = 6,
				min = 4
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 21,
				min = 52
			},
            {
				hour = 23,
				min = 40
			}
        },
        {
            {
				hour = 4,
				min = 25
			},
            {
				hour = 6,
				min = 5
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 21,
				min = 51
			},
            {
				hour = 23,
				min = 38
			}
        },
        {
            {
				hour = 4,
				min = 26
			},
            {
				hour = 6,
				min = 6
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 9
			},
            {
				hour = 21,
				min = 50
			},
            {
				hour = 23,
				min = 37
			}
        },
        {
            {
				hour = 4,
				min = 28
			},
            {
				hour = 6,
				min = 8
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 21,
				min = 49
			},
            {
				hour = 23,
				min = 35
			}
        },
        {
            {
				hour = 4,
				min = 30
			},
            {
				hour = 6,
				min = 9
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 21,
				min = 48
			},
            {
				hour = 23,
				min = 33
			}
        },
        {
            {
				hour = 4,
				min = 32
			},
            {
				hour = 6,
				min = 10
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 8
			},
            {
				hour = 21,
				min = 47
			},
            {
				hour = 23,
				min = 32
			}
        },
        {
            {
				hour = 4,
				min = 33
			},
            {
				hour = 6,
				min = 11
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 46
			},
            {
				hour = 23,
				min = 30
			}
        },
        {
            {
				hour = 4,
				min = 35
			},
            {
				hour = 6,
				min = 12
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 45
			},
            {
				hour = 23,
				min = 28
			}
        },
        {
            {
				hour = 4,
				min = 37
			},
            {
				hour = 6,
				min = 14
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 7
			},
            {
				hour = 21,
				min = 43
			},
            {
				hour = 23,
				min = 27
			}
        },
        {
            {
				hour = 4,
				min = 39
			},
            {
				hour = 6,
				min = 15
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 21,
				min = 42
			},
            {
				hour = 23,
				min = 25
			}
        },
        {
            {
				hour = 4,
				min = 41
			},
            {
				hour = 6,
				min = 16
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 6
			},
            {
				hour = 21,
				min = 41
			},
            {
				hour = 23,
				min = 23
			}
        },
        {
            {
				hour = 4,
				min = 42
			},
            {
				hour = 6,
				min = 17
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 5
			},
            {
				hour = 21,
				min = 40
			},
            {
				hour = 23,
				min = 21
			}
        },
        {
            {
				hour = 4,
				min = 44
			},
            {
				hour = 6,
				min = 19
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 5
			},
            {
				hour = 21,
				min = 38
			},
            {
				hour = 23,
				min = 19
			}
        },
        {
            {
				hour = 4,
				min = 46
			},
            {
				hour = 6,
				min = 20
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 21,
				min = 37
			},
            {
				hour = 23,
				min = 17
			}
        },
        {
            {
				hour = 4,
				min = 48
			},
            {
				hour = 6,
				min = 21
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 4
			},
            {
				hour = 21,
				min = 36
			},
            {
				hour = 23,
				min = 15
			}
        },
        {
            {
				hour = 4,
				min = 50
			},
            {
				hour = 6,
				min = 22
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 3
			},
            {
				hour = 21,
				min = 34
			},
            {
				hour = 23,
				min = 13
			}
        }
    },
    {
        {
            {
				hour = 4,
				min = 52
			},
            {
				hour = 6,
				min = 24
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 3
			},
            {
				hour = 21,
				min = 33
			},
            {
				hour = 23,
				min = 11
			}
        },
        {
            {
				hour = 4,
				min = 54
			},
            {
				hour = 6,
				min = 25
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 2
			},
            {
				hour = 21,
				min = 31
			},
            {
				hour = 23,
				min = 9
			}
        },
        {
            {
				hour = 4,
				min = 56
			},
            {
				hour = 6,
				min = 26
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 1
			},
            {
				hour = 21,
				min = 30
			},
            {
				hour = 23,
				min = 7
			}
        },
        {
            {
				hour = 4,
				min = 57
			},
            {
				hour = 6,
				min = 28
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 1
			},
            {
				hour = 21,
				min = 28
			},
            {
				hour = 23,
				min = 5
			}
        },
        {
            {
				hour = 4,
				min = 59
			},
            {
				hour = 6,
				min = 29
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 18,
				min = 0
			},
            {
				hour = 21,
				min = 27
			},
            {
				hour = 23,
				min = 3
			}
        },
        {
            {
				hour = 5,
				min = 1
			},
            {
				hour = 6,
				min = 31
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 17,
				min = 59
			},
            {
				hour = 21,
				min = 25
			},
            {
				hour = 23,
				min = 1
			}
        },
        {
            {
				hour = 5,
				min = 3
			},
            {
				hour = 6,
				min = 32
			},
            {
				hour = 14,
				min = 2
			},
            {
				hour = 17,
				min = 59
			},
            {
				hour = 21,
				min = 23
			},
            {
				hour = 22,
				min = 59
			}
        },
        {
            {
				hour = 5,
				min = 5
			},
            {
				hour = 6,
				min = 33
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 58
			},
            {
				hour = 21,
				min = 22
			},
            {
				hour = 22,
				min = 57
			}
        },
        {
            {
				hour = 5,
				min = 7
			},
            {
				hour = 6,
				min = 35
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 57
			},
            {
				hour = 21,
				min = 20
			},
            {
				hour = 22,
				min = 55
			}
        },
        {
            {
				hour = 5,
				min = 9
			},
            {
				hour = 6,
				min = 36
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 56
			},
            {
				hour = 21,
				min = 18
			},
            {
				hour = 22,
				min = 53
			}
        },
        {
            {
				hour = 5,
				min = 11
			},
            {
				hour = 6,
				min = 37
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 55
			},
            {
				hour = 21,
				min = 17
			},
            {
				hour = 22,
				min = 50
			}
        },
        {
            {
				hour = 5,
				min = 12
			},
            {
				hour = 6,
				min = 39
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 55
			},
            {
				hour = 21,
				min = 15
			},
            {
				hour = 22,
				min = 48
			}
        },
        {
            {
				hour = 5,
				min = 14
			},
            {
				hour = 6,
				min = 40
			},
            {
				hour = 14,
				min = 1
			},
            {
				hour = 17,
				min = 54
			},
            {
				hour = 21,
				min = 13
			},
            {
				hour = 22,
				min = 46
			}
        },
        {
            {
				hour = 5,
				min = 16
			},
            {
				hour = 6,
				min = 42
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 53
			},
            {
				hour = 21,
				min = 12
			},
            {
				hour = 22,
				min = 44
			}
        },
        {
            {
				hour = 5,
				min = 18
			},
            {
				hour = 6,
				min = 43
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 52
			},
            {
				hour = 21,
				min = 10
			},
            {
				hour = 22,
				min = 41
			}
        },
        {
            {
				hour = 5,
				min = 20
			},
            {
				hour = 6,
				min = 44
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 51
			},
            {
				hour = 21,
				min = 8
			},
            {
				hour = 22,
				min = 39
			}
        },
        {
            {
				hour = 5,
				min = 22
			},
            {
				hour = 6,
				min = 46
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 50
			},
            {
				hour = 21,
				min = 6
			},
            {
				hour = 22,
				min = 37
			}
        },
        {
            {
				hour = 5,
				min = 24
			},
            {
				hour = 6,
				min = 47
			},
            {
				hour = 14,
				min = 0
			},
            {
				hour = 17,
				min = 49
			},
            {
				hour = 21,
				min = 4
			},
            {
				hour = 22,
				min = 35
			}
        },
        {
            {
				hour = 5,
				min = 25
			},
            {
				hour = 6,
				min = 49
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 48
			},
            {
				hour = 21,
				min = 2
			},
            {
				hour = 22,
				min = 32
			}
        },
        {
            {
				hour = 5,
				min = 27
			},
            {
				hour = 6,
				min = 50
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 47
			},
            {
				hour = 21,
				min = 0
			},
            {
				hour = 22,
				min = 30
			}
        },
        {
            {
				hour = 5,
				min = 29
			},
            {
				hour = 6,
				min = 52
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 46
			},
            {
				hour = 20,
				min = 59
			},
            {
				hour = 22,
				min = 28
			}
        },
        {
            {
				hour = 5,
				min = 31
			},
            {
				hour = 6,
				min = 53
			},
            {
				hour = 13,
				min = 59
			},
            {
				hour = 17,
				min = 45
			},
            {
				hour = 20,
				min = 57
			},
            {
				hour = 22,
				min = 26
			}
        },
        {
            {
				hour = 5,
				min = 33
			},
            {
				hour = 6,
				min = 54
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 44
			},
            {
				hour = 20,
				min = 55
			},
            {
				hour = 22,
				min = 23
			}
        },
        {
            {
				hour = 5,
				min = 34
			},
            {
				hour = 6,
				min = 56
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 43
			},
            {
				hour = 20,
				min = 53
			},
            {
				hour = 22,
				min = 21
			}
        },
        {
            {
				hour = 5,
				min = 36
			},
            {
				hour = 6,
				min = 57
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 42
			},
            {
				hour = 20,
				min = 51
			},
            {
				hour = 22,
				min = 19
			}
        },
        {
            {
				hour = 5,
				min = 38
			},
            {
				hour = 6,
				min = 59
			},
            {
				hour = 13,
				min = 58
			},
            {
				hour = 17,
				min = 41
			},
            {
				hour = 20,
				min = 49
			},
            {
				hour = 22,
				min = 16
			}
        },
        {
            {
				hour = 5,
				min = 40
			},
            {
				hour = 7,
				min = 0
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 40
			},
            {
				hour = 20,
				min = 47
			},
            {
				hour = 22,
				min = 14
			}
        },
        {
            {
				hour = 5,
				min = 41
			},
            {
				hour = 7,
				min = 1
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 39
			},
            {
				hour = 20,
				min = 45
			},
            {
				hour = 22,
				min = 12
			}
        },
        {
            {
				hour = 5,
				min = 43
			},
            {
				hour = 7,
				min = 3
			},
            {
				hour = 13,
				min = 57
			},
            {
				hour = 17,
				min = 37
			},
            {
				hour = 20,
				min = 43
			},
            {
				hour = 22,
				min = 9
			}
        },
        {
            {
				hour = 5,
				min = 45
			},
            {
				hour = 7,
				min = 4
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 36
			},
            {
				hour = 20,
				min = 41
			},
            {
				hour = 22,
				min = 7
			}
        },
        {
            {
				hour = 5,
				min = 47
			},
            {
				hour = 7,
				min = 6
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 35
			},
            {
				hour = 20,
				min = 39
			},
            {
				hour = 22,
				min = 5
			}
        }
    },
    {
        {
            {
				hour = 5,
				min = 48
			},
            {
				hour = 7,
				min = 7
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 34
			},
            {
				hour = 20,
				min = 37
			},
            {
				hour = 22,
				min = 2
			}
        },
        {
            {
				hour = 5,
				min = 50
			},
            {
				hour = 7,
				min = 8
			},
            {
				hour = 13,
				min = 56
			},
            {
				hour = 17,
				min = 33
			},
            {
				hour = 20,
				min = 35
			},
            {
				hour = 22,
				min = 0
			}
        },
        {
            {
				hour = 5,
				min = 52
			},
            {
				hour = 7,
				min = 10
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 31
			},
            {
				hour = 20,
				min = 33
			},
            {
				hour = 21,
				min = 58
			}
        },
        {
            {
				hour = 5,
				min = 53
			},
            {
				hour = 7,
				min = 11
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 30
			},
            {
				hour = 20,
				min = 31
			},
            {
				hour = 21,
				min = 55
			}
        },
        {
            {
				hour = 5,
				min = 55
			},
            {
				hour = 7,
				min = 13
			},
            {
				hour = 13,
				min = 55
			},
            {
				hour = 17,
				min = 29
			},
            {
				hour = 20,
				min = 29
			},
            {
				hour = 21,
				min = 53
			}
        },
        {
            {
				hour = 5,
				min = 57
			},
            {
				hour = 7,
				min = 14
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 27
			},
            {
				hour = 20,
				min = 26
			},
            {
				hour = 21,
				min = 51
			}
        },
        {
            {
				hour = 5,
				min = 58
			},
            {
				hour = 7,
				min = 16
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 26
			},
            {
				hour = 20,
				min = 24
			},
            {
				hour = 21,
				min = 48
			}
        },
        {
            {
				hour = 6,
				min = 0
			},
            {
				hour = 7,
				min = 17
			},
            {
				hour = 13,
				min = 54
			},
            {
				hour = 17,
				min = 25
			},
            {
				hour = 20,
				min = 22
			},
            {
				hour = 21,
				min = 46
			}
        },
        {
            {
				hour = 6,
				min = 2
			},
            {
				hour = 7,
				min = 18
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 23
			},
            {
				hour = 20,
				min = 20
			},
            {
				hour = 21,
				min = 44
			}
        },
        {
            {
				hour = 6,
				min = 3
			},
            {
				hour = 7,
				min = 20
			},
            {
				hour = 13,
				min = 53
			},
            {
				hour = 17,
				min = 22
			},
            {
				hour = 20,
				min = 18
			},
            {
				hour = 21,
				min = 41
			}
        },
        {
            {
				hour = 6,
				min = 5
			},
            {
				hour = 7,
				min = 21
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 21
			},
            {
				hour = 20,
				min = 16
			},
            {
				hour = 21,
				min = 39
			}
        },
        {
            {
				hour = 6,
				min = 6
			},
            {
				hour = 7,
				min = 23
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 19
			},
            {
				hour = 20,
				min = 14
			},
            {
				hour = 21,
				min = 37
			}
        },
        {
            {
				hour = 6,
				min = 8
			},
            {
				hour = 7,
				min = 24
			},
            {
				hour = 13,
				min = 52
			},
            {
				hour = 17,
				min = 18
			},
            {
				hour = 20,
				min = 12
			},
            {
				hour = 21,
				min = 35
			}
        },
        {
            {
				hour = 6,
				min = 10
			},
            {
				hour = 7,
				min = 25
			},
            {
				hour = 13,
				min = 51
			},
            {
				hour = 17,
				min = 17
			},
            {
				hour = 20,
				min = 10
			},
            {
				hour = 21,
				min = 32
			}
        },
        {
            {
				hour = 6,
				min = 11
			},
            {
				hour = 7,
				min = 27
			},
            {
				hour = 13,
				min = 51
			},
            {
				hour = 17,
				min = 15
			},
            {
				hour = 20,
				min = 7
			},
            {
				hour = 21,
				min = 30
			}
        },
        {
            {
				hour = 6,
				min = 13
			},
            {
				hour = 7,
				min = 28
			},
            {
				hour = 13,
				min = 51
			},
            {
				hour = 17,
				min = 14
			},
            {
				hour = 20,
				min = 5
			},
            {
				hour = 21,
				min = 28
			}
        },
        {
            {
				hour = 6,
				min = 14
			},
            {
				hour = 7,
				min = 30
			},
            {
				hour = 13,
				min = 50
			},
            {
				hour = 17,
				min = 12
			},
            {
				hour = 20,
				min = 3
			},
            {
				hour = 21,
				min = 25
			}
        },
        {
            {
				hour = 6,
				min = 16
			},
            {
				hour = 7,
				min = 31
			},
            {
				hour = 13,
				min = 50
			},
            {
				hour = 17,
				min = 11
			},
            {
				hour = 20,
				min = 1
			},
            {
				hour = 21,
				min = 23
			}
        },
        {
            {
				hour = 6,
				min = 17
			},
            {
				hour = 7,
				min = 32
			},
            {
				hour = 13,
				min = 50
			},
            {
				hour = 17,
				min = 9
			},
            {
				hour = 19,
				min = 59
			},
            {
				hour = 21,
				min = 21
			}
        },
        {
            {
				hour = 6,
				min = 19
			},
            {
				hour = 7,
				min = 34
			},
            {
				hour = 13,
				min = 49
			},
            {
				hour = 17,
				min = 8
			},
            {
				hour = 19,
				min = 57
			},
            {
				hour = 21,
				min = 19
			}
        },
        {
            {
				hour = 6,
				min = 20
			},
            {
				hour = 7,
				min = 35
			},
            {
				hour = 13,
				min = 49
			},
            {
				hour = 17,
				min = 6
			},
            {
				hour = 19,
				min = 55
			},
            {
				hour = 21,
				min = 16
			}
        },
        {
            {
				hour = 6,
				min = 22
			},
            {
				hour = 7,
				min = 37
			},
            {
				hour = 13,
				min = 49
			},
            {
				hour = 17,
				min = 5
			},
            {
				hour = 19,
				min = 52
			},
            {
				hour = 21,
				min = 14
			}
        },
        {
            {
				hour = 6,
				min = 24
			},
            {
				hour = 7,
				min = 38
			},
            {
				hour = 13,
				min = 48
			},
            {
				hour = 17,
				min = 4
			},
            {
				hour = 19,
				min = 50
			},
            {
				hour = 21,
				min = 12
			}
        },
        {
            {
				hour = 6,
				min = 25
			},
            {
				hour = 7,
				min = 40
			},
            {
				hour = 13,
				min = 48
			},
            {
				hour = 17,
				min = 2
			},
            {
				hour = 19,
				min = 48
			},
            {
				hour = 21,
				min = 10
			}
        },
        {
            {
				hour = 6,
				min = 27
			},
            {
				hour = 7,
				min = 41
			},
            {
				hour = 13,
				min = 48
			},
            {
				hour = 17,
				min = 1
			},
            {
				hour = 19,
				min = 46
			},
            {
				hour = 21,
				min = 7
			}
        },
        {
            {
				hour = 6,
				min = 28
			},
            {
				hour = 7,
				min = 42
			},
            {
				hour = 13,
				min = 47
			},
            {
				hour = 16,
				min = 59
			},
            {
				hour = 19,
				min = 44
			},
            {
				hour = 21,
				min = 5
			}
        },
        {
            {
				hour = 6,
				min = 30
			},
            {
				hour = 7,
				min = 44
			},
            {
				hour = 13,
				min = 47
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 42
			},
            {
				hour = 21,
				min = 3
			}
        },
        {
            {
				hour = 6,
				min = 31
			},
            {
				hour = 7,
				min = 45
			},
            {
				hour = 13,
				min = 46
			},
            {
				hour = 16,
				min = 56
			},
            {
				hour = 19,
				min = 40
			},
            {
				hour = 21,
				min = 1
			}
        },
        {
            {
				hour = 6,
				min = 33
			},
            {
				hour = 7,
				min = 47
			},
            {
				hour = 13,
				min = 46
			},
            {
				hour = 16,
				min = 55
			},
            {
				hour = 19,
				min = 38
			},
            {
				hour = 20,
				min = 59
			}
        },
        {
            {
				hour = 6,
				min = 34
			},
            {
				hour = 7,
				min = 48
			},
            {
				hour = 13,
				min = 46
			},
            {
				hour = 16,
				min = 53
			},
            {
				hour = 19,
				min = 36
			},
            {
				hour = 20,
				min = 57
			}
        }
    },
    {
        {
            {
				hour = 6,
				min = 36
			},
            {
				hour = 7,
				min = 50
			},
            {
				hour = 13,
				min = 45
			},
            {
				hour = 16,
				min = 52
			},
            {
				hour = 19,
				min = 33
			},
            {
				hour = 20,
				min = 54
			}
        },
        {
            {
				hour = 6,
				min = 37
			},
            {
				hour = 7,
				min = 51
			},
            {
				hour = 13,
				min = 45
			},
            {
				hour = 16,
				min = 50
			},
            {
				hour = 19,
				min = 31
			},
            {
				hour = 20,
				min = 52
			}
        },
        {
            {
				hour = 6,
				min = 39
			},
            {
				hour = 7,
				min = 53
			},
            {
				hour = 13,
				min = 45
			},
            {
				hour = 16,
				min = 49
			},
            {
				hour = 19,
				min = 29
			},
            {
				hour = 20,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 40
			},
            {
				hour = 7,
				min = 54
			},
            {
				hour = 13,
				min = 45
			},
            {
				hour = 16,
				min = 47
			},
            {
				hour = 19,
				min = 27
			},
            {
				hour = 20,
				min = 48
			}
        },
        {
            {
				hour = 6,
				min = 41
			},
            {
				hour = 7,
				min = 56
			},
            {
				hour = 13,
				min = 44
			},
            {
				hour = 16,
				min = 45
			},
            {
				hour = 19,
				min = 25
			},
            {
				hour = 20,
				min = 46
			}
        },
        {
            {
				hour = 6,
				min = 43
			},
            {
				hour = 7,
				min = 57
			},
            {
				hour = 13,
				min = 44
			},
            {
				hour = 16,
				min = 44
			},
            {
				hour = 19,
				min = 23
			},
            {
				hour = 20,
				min = 44
			}
        },
        {
            {
				hour = 6,
				min = 44
			},
            {
				hour = 7,
				min = 58
			},
            {
				hour = 13,
				min = 44
			},
            {
				hour = 16,
				min = 42
			},
            {
				hour = 19,
				min = 21
			},
            {
				hour = 20,
				min = 42
			}
        },
        {
            {
				hour = 6,
				min = 46
			},
            {
				hour = 8,
				min = 0
			},
            {
				hour = 13,
				min = 43
			},
            {
				hour = 16,
				min = 41
			},
            {
				hour = 19,
				min = 19
			},
            {
				hour = 20,
				min = 40
			}
        },
        {
            {
				hour = 6,
				min = 47
			},
            {
				hour = 8,
				min = 1
			},
            {
				hour = 13,
				min = 43
			},
            {
				hour = 16,
				min = 39
			},
            {
				hour = 19,
				min = 17
			},
            {
				hour = 20,
				min = 38
			}
        },
        {
            {
				hour = 6,
				min = 49
			},
            {
				hour = 8,
				min = 3
			},
            {
				hour = 13,
				min = 43
			},
            {
				hour = 16,
				min = 38
			},
            {
				hour = 19,
				min = 15
			},
            {
				hour = 20,
				min = 36
			}
        },
        {
            {
				hour = 6,
				min = 50
			},
            {
				hour = 8,
				min = 4
			},
            {
				hour = 13,
				min = 43
			},
            {
				hour = 16,
				min = 36
			},
            {
				hour = 19,
				min = 13
			},
            {
				hour = 20,
				min = 34
			}
        },
        {
            {
				hour = 6,
				min = 52
			},
            {
				hour = 8,
				min = 6
			},
            {
				hour = 13,
				min = 42
			},
            {
				hour = 16,
				min = 35
			},
            {
				hour = 19,
				min = 11
			},
            {
				hour = 20,
				min = 32
			}
        },
        {
            {
				hour = 6,
				min = 53
			},
            {
				hour = 8,
				min = 7
			},
            {
				hour = 13,
				min = 42
			},
            {
				hour = 16,
				min = 33
			},
            {
				hour = 19,
				min = 9
			},
            {
				hour = 20,
				min = 30
			}
        },
        {
            {
				hour = 6,
				min = 55
			},
            {
				hour = 8,
				min = 9
			},
            {
				hour = 13,
				min = 42
			},
            {
				hour = 16,
				min = 32
			},
            {
				hour = 19,
				min = 7
			},
            {
				hour = 20,
				min = 28
			}
        },
        {
            {
				hour = 6,
				min = 56
			},
            {
				hour = 8,
				min = 10
			},
            {
				hour = 13,
				min = 42
			},
            {
				hour = 16,
				min = 30
			},
            {
				hour = 19,
				min = 5
			},
            {
				hour = 20,
				min = 26
			}
        },
        {
            {
				hour = 6,
				min = 58
			},
            {
				hour = 8,
				min = 12
			},
            {
				hour = 13,
				min = 41
			},
            {
				hour = 16,
				min = 29
			},
            {
				hour = 19,
				min = 3
			},
            {
				hour = 20,
				min = 24
			}
        },
        {
            {
				hour = 6,
				min = 59
			},
            {
				hour = 8,
				min = 13
			},
            {
				hour = 13,
				min = 41
			},
            {
				hour = 16,
				min = 28
			},
            {
				hour = 19,
				min = 1
			},
            {
				hour = 20,
				min = 22
			}
        },
        {
            {
				hour = 7,
				min = 0
			},
            {
				hour = 8,
				min = 15
			},
            {
				hour = 13,
				min = 41
			},
            {
				hour = 16,
				min = 26
			},
            {
				hour = 18,
				min = 59
			},
            {
				hour = 20,
				min = 21
			}
        },
        {
            {
				hour = 7,
				min = 2
			},
            {
				hour = 8,
				min = 17
			},
            {
				hour = 13,
				min = 41
			},
            {
				hour = 16,
				min = 25
			},
            {
				hour = 18,
				min = 57
			},
            {
				hour = 20,
				min = 19
			}
        },
        {
            {
				hour = 7,
				min = 3
			},
            {
				hour = 8,
				min = 18
			},
            {
				hour = 13,
				min = 41
			},
            {
				hour = 16,
				min = 23
			},
            {
				hour = 18,
				min = 55
			},
            {
				hour = 20,
				min = 17
			}
        },
        {
            {
				hour = 7,
				min = 5
			},
            {
				hour = 8,
				min = 20
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 22
			},
            {
				hour = 18,
				min = 53
			},
            {
				hour = 20,
				min = 15
			}
        },
        {
            {
				hour = 7,
				min = 6
			},
            {
				hour = 8,
				min = 21
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 20
			},
            {
				hour = 18,
				min = 51
			},
            {
				hour = 20,
				min = 13
			}
        },
        {
            {
				hour = 7,
				min = 8
			},
            {
				hour = 8,
				min = 23
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 19
			},
            {
				hour = 18,
				min = 50
			},
            {
				hour = 20,
				min = 12
			}
        },
        {
            {
				hour = 7,
				min = 9
			},
            {
				hour = 8,
				min = 24
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 18
			},
            {
				hour = 18,
				min = 48
			},
            {
				hour = 20,
				min = 10
			}
        },
        {
            {
				hour = 7,
				min = 11
			},
            {
				hour = 8,
				min = 26
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 16
			},
            {
				hour = 18,
				min = 46
			},
            {
				hour = 20,
				min = 8
			}
        },
        {
            {
				hour = 7,
				min = 12
			},
            {
				hour = 8,
				min = 27
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 15
			},
            {
				hour = 18,
				min = 44
			},
            {
				hour = 20,
				min = 7
			}
        },
        {
            {
				hour = 7,
				min = 13
			},
            {
				hour = 8,
				min = 29
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 14
			},
            {
				hour = 18,
				min = 42
			},
            {
				hour = 20,
				min = 5
			}
        },
        {
            {
				hour = 7,
				min = 15
			},
            {
				hour = 8,
				min = 31
			},
            {
				hour = 13,
				min = 40
			},
            {
				hour = 16,
				min = 12
			},
            {
				hour = 18,
				min = 41
			},
            {
				hour = 20,
				min = 3
			}
        },
        {
            {
				hour = 6,
				min = 16
			},
            {
				hour = 7,
				min = 32
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 11
			},
            {
				hour = 17,
				min = 39
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 18
			},
            {
				hour = 7,
				min = 34
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 10
			},
            {
				hour = 17,
				min = 37
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 19
			},
            {
				hour = 7,
				min = 35
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 8
			},
            {
				hour = 17,
				min = 36
			},
            {
				hour = 19,
				min = 50
			}
        }
    },
    {
        {
            {
				hour = 6,
				min = 21
			},
            {
				hour = 7,
				min = 37
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 7
			},
            {
				hour = 17,
				min = 34
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 22
			},
            {
				hour = 7,
				min = 38
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 6
			},
            {
				hour = 17,
				min = 32
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 23
			},
            {
				hour = 7,
				min = 40
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 5
			},
            {
				hour = 17,
				min = 31
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 25
			},
            {
				hour = 7,
				min = 42
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 3
			},
            {
				hour = 17,
				min = 29
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 26
			},
            {
				hour = 7,
				min = 43
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 2
			},
            {
				hour = 17,
				min = 28
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 28
			},
            {
				hour = 7,
				min = 45
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 1
			},
            {
				hour = 17,
				min = 26
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 29
			},
            {
				hour = 7,
				min = 46
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 15,
				min = 0
			},
            {
				hour = 17,
				min = 25
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 30
			},
            {
				hour = 7,
				min = 48
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 59
			},
            {
				hour = 17,
				min = 23
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 32
			},
            {
				hour = 7,
				min = 50
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 58
			},
            {
				hour = 17,
				min = 22
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 33
			},
            {
				hour = 7,
				min = 51
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 57
			},
            {
				hour = 17,
				min = 20
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 35
			},
            {
				hour = 7,
				min = 53
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 56
			},
            {
				hour = 17,
				min = 19
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 36
			},
            {
				hour = 7,
				min = 54
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 55
			},
            {
				hour = 17,
				min = 18
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 37
			},
            {
				hour = 7,
				min = 56
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 54
			},
            {
				hour = 17,
				min = 16
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 39
			},
            {
				hour = 7,
				min = 57
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 53
			},
            {
				hour = 17,
				min = 15
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 40
			},
            {
				hour = 7,
				min = 59
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 52
			},
            {
				hour = 17,
				min = 14
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 41
			},
            {
				hour = 8,
				min = 1
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 51
			},
            {
				hour = 17,
				min = 13
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 43
			},
            {
				hour = 8,
				min = 2
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 50
			},
            {
				hour = 17,
				min = 12
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 44
			},
            {
				hour = 8,
				min = 4
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 49
			},
            {
				hour = 17,
				min = 10
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 45
			},
            {
				hour = 8,
				min = 5
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 49
			},
            {
				hour = 17,
				min = 9
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 47
			},
            {
				hour = 8,
				min = 7
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 48
			},
            {
				hour = 17,
				min = 8
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 48
			},
            {
				hour = 8,
				min = 8
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 47
			},
            {
				hour = 17,
				min = 7
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 49
			},
            {
				hour = 8,
				min = 10
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 46
			},
            {
				hour = 17,
				min = 6
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 51
			},
            {
				hour = 8,
				min = 11
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 46
			},
            {
				hour = 17,
				min = 6
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 52
			},
            {
				hour = 8,
				min = 12
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 45
			},
            {
				hour = 17,
				min = 5
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 53
			},
            {
				hour = 8,
				min = 14
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 44
			},
            {
				hour = 17,
				min = 4
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 54
			},
            {
				hour = 8,
				min = 15
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 44
			},
            {
				hour = 17,
				min = 3
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 56
			},
            {
				hour = 8,
				min = 17
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 43
			},
            {
				hour = 17,
				min = 2
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 57
			},
            {
				hour = 8,
				min = 18
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 43
			},
            {
				hour = 17,
				min = 2
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 58
			},
            {
				hour = 8,
				min = 19
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 17,
				min = 1
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 6,
				min = 59
			},
            {
				hour = 8,
				min = 21
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 17,
				min = 0
			},
            {
				hour = 19,
				min = 50
			}
        }
    },
    {
        {
            {
				hour = 7,
				min = 0
			},
            {
				hour = 8,
				min = 22
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 17,
				min = 0
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 1
			},
            {
				hour = 8,
				min = 23
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 59
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 2
			},
            {
				hour = 8,
				min = 25
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 59
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 3
			},
            {
				hour = 8,
				min = 26
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 5
			},
            {
				hour = 8,
				min = 27
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 6
			},
            {
				hour = 8,
				min = 28
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 7
			},
            {
				hour = 8,
				min = 29
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 8
			},
            {
				hour = 8,
				min = 30
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 8
			},
            {
				hour = 8,
				min = 31
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 9
			},
            {
				hour = 8,
				min = 32
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 10
			},
            {
				hour = 8,
				min = 33
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 11
			},
            {
				hour = 8,
				min = 34
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 12
			},
            {
				hour = 8,
				min = 35
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 13
			},
            {
				hour = 8,
				min = 36
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 13
			},
            {
				hour = 8,
				min = 37
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 57
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 14
			},
            {
				hour = 8,
				min = 38
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 15
			},
            {
				hour = 8,
				min = 38
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 40
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 16
			},
            {
				hour = 8,
				min = 39
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 16
			},
            {
				hour = 8,
				min = 40
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 58
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 17
			},
            {
				hour = 8,
				min = 40
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 41
			},
            {
				hour = 16,
				min = 59
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 17
			},
            {
				hour = 8,
				min = 41
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 16,
				min = 59
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 18
			},
            {
				hour = 8,
				min = 42
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 17,
				min = 0
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 18
			},
            {
				hour = 8,
				min = 42
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 42
			},
            {
				hour = 17,
				min = 0
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 19
			},
            {
				hour = 8,
				min = 42
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 43
			},
            {
				hour = 17,
				min = 1
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 19
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 43
			},
            {
				hour = 17,
				min = 2
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 44
			},
            {
				hour = 17,
				min = 2
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 43
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 45
			},
            {
				hour = 17,
				min = 3
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 45
			},
            {
				hour = 17,
				min = 4
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 20
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 46
			},
            {
				hour = 17,
				min = 5
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 47
			},
            {
				hour = 17,
				min = 6
			},
            {
				hour = 19,
				min = 50
			}
        },
        {
            {
				hour = 7,
				min = 21
			},
            {
				hour = 8,
				min = 44
			},
            {
				hour = 13,
				min = 0
			},
            {
				hour = 14,
				min = 47
			},
            {
				hour = 17,
				min = 6
			},
            {
				hour = 19,
				min = 50
			}
        }
    }
}

return times
