local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local naughty = require("naughty")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local battery_widget = wibox.widget({
    {
        id = "battery",
        widget = wibox.widget.textbox,
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    battery_low_icon = string.format("<span foreground='%s'>  </span>", beautiful.color9),
    status_indicators = {
        ["Full"] = "F",
        ["Discharging"] = "D",
        ["Charging"] = "C",
        ["Not charging"] = "N",
        ["Unknown"] = "U"
    },
    battery_string_template = "<span foreground='%s'>bat%d</span> %s%3d:%s",
    battery_fetch_command = "acpi | sed 's/.*: \\(.*\\), \\([[:digit:]]\\+\\)%.*/\\1,\\2/'",
})

battery_widget:buttons(gears.table.join(
    awful.button(
        { },
        1,
        function()
            awful.spawn.easy_async(
                "acpi",
                function(stdout)
                    naughty.notify({
                        text = stdout,
                    })
                end
            )
        end
    ),
    awful.button(
        { },
        3,
        function()
            naughty.notify({
                text = [[
F : full
D : discharging
C : charging
N : not charging
U : unknown
 : battery very low!"
                ]],
                title = "Battery module"
            })
        end
    )
))

function battery_widget:set_info(info)
    local child = self:get_children_by_id("battery")[1]
    local status = {}
    local total_level = 0
    local is_charging = false

    for k, battery_info in pairs(info) do
        local warn = ""
        local level = tonumber(battery_info[2])

        if level <= 25 then
            warn = self.battery_low_icon
        end

        if battery_info[1] == "Charging" then
            is_charging = true
        end

        status[#status + 1] = string.format(
            self.battery_string_template,
            beautiful.color7,
            k,
            warn,
            level,
            self.status_indicators[battery_info[1]]
        )
        total_level = total_level + level
    end
    child:set_markup_silently(table.concat(status, " - "))

    if total_level <= 25 and not is_charging then
        naughty.notify({
		    preset = naughty.config.presets.critical,
            text = "Low battery !!",
        })
    end
end

function battery_widget:check_battery()
    awful.spawn.easy_async_with_shell(
        self.battery_fetch_command,
        function(stdout)
            local info = {}
            local k = 1

            for battery_info in string.gmatch(stdout, "[^\r\n]+") do
                info[k] = {}
                for v in string.gmatch(battery_info, "[^,]+") do
                    table.insert(info[k], v)
                end
                k = k + 1
            end

            pcall(self.set_info, self, info)
        end
    )
end

return battery_widget
