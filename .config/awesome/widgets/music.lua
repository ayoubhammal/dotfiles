local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")
beautiful.init("~/.config/awesome/theme.lua")

local music_widget = wibox.widget({
    {
        {
            id = "music_text",
            widget = wibox.widget.textbox,
            markup = string.format("<span foreground='%s'>mpd </span>", beautiful.color7),
        },
        {
            {
                id = "music_current",
                ellipsize = "end",
                widget = wibox.widget.textbox,
            },
            width = 200,
            widget = wibox.container.constraint,
        },
        layout = wibox.layout.align.horizontal
    },
    layout = wibox.container.margin,
    left = 10,
    right = 10,
    music_current_command = "mpc current",
})

function music_widget:set_title(title)
    local child = self:get_children_by_id("music_current")[1]
    if child.title ~= title then
        child.title = title 
        child:set_markup_silently(title)
    end
end

function music_widget:check_title()
    awful.spawn.easy_async(
        self.music_current_command,
        function(stdout, stderr)
            pcall(self.set_title, self, stdout)
        end
    )
end

return music_widget 
