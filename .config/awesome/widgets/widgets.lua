local gears = require("gears")
local wibox = require("wibox")
local awful = require("awful")

local mic_widget = require("widgets.mic")
local audio_widget = require("widgets.audio")
local battery_widget = require("widgets.battery")
local brightness_widget = require("widgets.brightness")
local caps_widget = require("widgets.caps")
local prayer_widget = require("widgets.prayer")
local music_widget = require("widgets.music")

gears.timer({
    timeout = 120,
    autostart = true,
    call_now = true,
    callback = function()
        battery_widget:check_battery()
        prayer_widget:check_prayer()
    end
})

gears.timer({
    timeout = 30,
    autostart = true,
    call_now = true,
    callback = function()
        music_widget:check_title()
        brightness_widget:check_level()
    end
})

gears.timer({
    timeout = 5,
    autostart = true,
    call_now = true,
    callback = function()
        mic_widget:check_mic()
        audio_widget:check_audio()
        caps_widget:check_lock()
    end
})

local mytextclock = wibox.widget({
	{
		widget = wibox.widget.textclock("%A, %B %d, %Y - %H:%M:%S", 5),
	},
	layout = wibox.container.margin,
	left = 10,
	right = 10,
})
mytextclock:buttons(awful.button({}, 1, function()
	awful.spawn.easy_async("gsimplecal", function() end)
end))

local myseparator = wibox.widget({
	widget = wibox.widget.separator,
	orientation = "vertical",
    opacity = 0.5,
    span_ratio = 0.5,
    thickness = 0.5,
    forced_width = 10,
})

return {mytextclock, mic_widget, audio_widget, battery_widget, brightness_widget, caps_widget, prayer_widget, music_widget, myseparator}
