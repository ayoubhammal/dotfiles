-- Mappings
vim.g.mapleader = " "
vim.g.maplocalleader = "\\"

vim.api.nvim_set_keymap('', '<Up>', '', {})
vim.api.nvim_set_keymap('', '<Down>', '', {})
vim.api.nvim_set_keymap('', '<Left>', '', {})
vim.api.nvim_set_keymap('', '<Right>', '', {})

vim.api.nvim_set_keymap('n', '<c-a-l>', '<c-w><c-L>', { noremap = true})
vim.api.nvim_set_keymap('n', '<c-a-h>', '<c-w><c-H>', { noremap = true})
vim.api.nvim_set_keymap('n', '<c-a-k>', '<c-w><c-K>', { noremap = true})
vim.api.nvim_set_keymap('n', '<c-a-j>', '<c-w><c-J>', { noremap = true})

-- Buffer mapping
vim.api.nvim_set_keymap('n', '<leader>;', '<cmd>bn<cr>', { noremap = true })
vim.api.nvim_set_keymap('n', '<leader>,', '<cmd>bp<cr>', { noremap = true })
vim.api.nvim_set_keymap('n', '<leader>d', '<cmd>bd<cr>', { noremap = true })

vim.api.nvim_set_keymap('v', '<', '<gv', { noremap = true })
vim.api.nvim_set_keymap('v', '>', '>gv', { noremap = true })
