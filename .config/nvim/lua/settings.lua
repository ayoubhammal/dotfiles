-- Settings
vim.opt.syntax = 'enable'
vim.opt.title = true
vim.opt.smarttab = true
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.mouse = 'a'
vim.opt.hidden = true
vim.opt.errorbells = false
vim.opt.scrolloff = 5
vim.opt.clipboard = vim.api.nvim_get_option('clipboard')..'unnamedplus'
vim.opt.showmode = true
vim.opt.showcmd = true
vim.opt.cmdheight = 2
vim.opt.ruler = true
vim.opt.laststatus = 2
vim.opt.completeopt = 'menuone,noinsert,noselect'
vim.opt.updatetime = 50
vim.opt.undodir = vim.fn.stdpath('cache') .. '/undo'
vim.opt.undolevels = 1000
vim.opt.undofile =  true

vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 4
vim.opt.autoindent = true
vim.opt.smartindent = true
vim.opt.expandtab = true

vim.opt.wrap = true
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.opt.linebreak = true
vim.opt.signcolumn = 'yes'
vim.opt.colorcolumn = '80'
vim.opt.termguicolors = true
