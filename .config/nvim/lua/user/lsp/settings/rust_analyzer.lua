return {
    assist = {
        importGranularity = "module",
        importPrefix = "self",
    },
    cargo = {
        loadOutDirsFromCheck = true
    },
    procMacro = {
        enable = true
    }
}
