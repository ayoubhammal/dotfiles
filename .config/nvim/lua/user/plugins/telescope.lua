return {
    {
        'nvim-telescope/telescope.nvim',
        dependencies = {
            'nvim-lua/plenary.nvim'
        },
        config = function ()
            local builtin = require("telescope.builtin")
            -- Telescope mapping
            vim.keymap.set('n', '<leader>ff', builtin.find_files, { noremap = true })
            vim.keymap.set('n', '<leader>fg', builtin.live_grep, { noremap = true })
            vim.keymap.set('n', '<leader>fb', builtin.buffers, { noremap = true })
            vim.keymap.set('n', '<leader>fh', builtin.help_tags, { noremap = true })
        end
    },
}
