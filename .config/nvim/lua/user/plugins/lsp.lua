-- Change diagnostic symbols in the sign column (gutter)
-- local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
-- for type, icon in pairs(signs) do
--     local hl = "DiagnosticSign" .. type
--     vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
-- end

-- Customizing how diagnostics are displayed
vim.diagnostic.config({
    virtual_text = false,
    signs = true,
    underline = true,
    update_in_insert = true,
    severity_sort = false,
    float = {
        focusable = false,
        style = "minimal",
        border = "rounded",
        source = "always",
        header = "",
        prefix = "",
    },
})

return {
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            {
                "williamboman/mason.nvim",
                config = function ()
                    require("mason").setup()
                end
            },
            {
                "williamboman/mason-lspconfig.nvim",
                config = function ()
                    local mason_lspconfig = require("mason-lspconfig")

                    mason_lspconfig.setup()

                    local function lsp_keymaps (bufnr)
                        local opts = { noremap=true, silent=true }

                        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
                        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
                        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
                        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
                        vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)

                        -- vim.api.nvim_set_keymap('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)

                        vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)

                        -- vim.api.nvim_set_keymap('n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

                        vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)

                        -- vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
                        -- vim.api.nvim_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
                    end

                    local opts = {
                        on_attach = function (client, bufnr)
                            lsp_keymaps(bufnr)
                        end,
                        capabilities = require("cmp_nvim_lsp").default_capabilities(
                            vim.lsp.protocol.make_client_capabilities()
                        )
                    }
                    mason_lspconfig.setup_handlers {
                        function (server_name)
                            require("lspconfig")[server_name].setup(opts)
                        end,
                        ["lua_ls"] = function ()
                            opts["settings"] = require("user.lsp.settings.sumneko_lua")
                            require("lspconfig")["lua_ls"].setup(opts)
                        end,
                        ["rust_analyzer"] = function ()
                            opts["settings"] = require("user.lsp.settings.rust_analyzer")
                            require("lspconfig")["rust_analyzer"].setup(opts)
                        end,
                        ["zls"] = function ()
                            opts["settings"] = require("user.lsp.settings.zls")
                            require("lspconfig")["zls"].setup(opts)
                        end,
                        ["basedpyright"] = function ()
                            opts["settings"] = require("user.lsp.settings.basedpyright")
                            require("lspconfig")["basedpyright"].setup(opts)
                        end
                    }
                end
            }
        },
    },
}
