return {
    "akinsho/toggleterm.nvim",
    config = function ()
        -- Togggle terminal setup
        require("toggleterm").setup{
            open_mapping = [[<c-\>]],
            direction = 'float',
            float_opts = {
                border = 'curved',
            }
        }

        local Terminal  = require('toggleterm.terminal').Terminal
        local lazygit = Terminal:new({ cmd = "lazygit", hidden = true })

        local function _lazygit_toggle ()
          lazygit:toggle()
        end

        -- Toggle termainal
        vim.keymap.set("n", "<c-^>", _lazygit_toggle, {noremap = true, silent = true})
    end
}
