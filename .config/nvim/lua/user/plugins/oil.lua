return {
    {
        'stevearc/oil.nvim',
        opts = {},
        config = function ()
            require("oil").setup()
            vim.keymap.set("n", "<leader>-", "<cmd>Oil<cr>", { desc = "Open parent directory" })
        end
    },
}
