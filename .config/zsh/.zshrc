autoload -U colors && colors	

PS1="
╭─ %F{blue}[%f %B%F{red}%n%f%b %F{yellow}@%f %B%F{green}%m%f%b %F{magenta}%~%f %F{blue}]%f
╰─ $ "

HISTSIZE=1000
SAVEHIST=1000
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/aliasrc"

autoload -Uz compinit
zstyle ':completion:*' menu select
compinit

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"

bindkey '^ ' autosuggest-accept

eval "$(fzf --zsh)"

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/ayoub/miniforge3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/ayoub/miniforge3/etc/profile.d/conda.sh" ]; then
        . "/home/ayoub/miniforge3/etc/profile.d/conda.sh"
    else
        export PATH="/home/ayoub/miniforge3/bin:$PATH"
    fi
fi
unset __conda_setup

if [ -f "/home/ayoub/miniforge3/etc/profile.d/mamba.sh" ]; then
    . "/home/ayoub/miniforge3/etc/profile.d/mamba.sh"
fi
# <<< conda initialize <<<

