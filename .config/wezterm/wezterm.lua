local wezterm = require('wezterm')

local config = wezterm.config_builder()


config.font = wezterm.font("monospace")
config.font_size = 9.0

config.enable_tab_bar = false
config.enable_scroll_bar = false

config.window_padding = {
    left = 0,
    right = 0,
    top = 0,
    bottom = 0,
}


config.colors = {
    foreground = "#fbf1c7",
    background = "#1d2021",

    cursor_bg = '#fbf1c7',
    cursor_fg = '#1d2021',

    cursor_border = '#fbf1c7',

    selection_fg = '#1d2021',
    selection_bg = '#a89984',

    -- The color of the split lines between panes
    split = '#444444',
    ansi = {
        '#282828',
        '#cc241d',
        '#98971a',
        '#d65d0e',
        '#458588',
        '#b16286',
        '#689d6a',
        '#a89984',
    },
    brights = {
        "#928374",
        "#fb4934",
        "#b8bb26",
        "#fe8019",
        "#83a598",
        "#d3869b",
        "#8ec07c",
        "#ebdbb2",
    },
    compose_cursor = 'orange',
}

config.disable_default_key_bindings = true

config.keys = {
    {
        key = "=",
        mods ="CTRL",
        action = wezterm.action.IncreaseFontSize,
    },
    {
        key = "-",
        mods ="CTRL",
        action = wezterm.action.DecreaseFontSize,
    },
    {
        key = "0",
        mods ="CTRL",
        action = wezterm.action.ResetFontSize,
    },
    {
        key = "c",
        mods ="CTRL|SHIFT",
        action = wezterm.action.CopyTo("Clipboard"),
    },
    {
        key = "v",
        mods ="CTRL|SHIFT",
        action = wezterm.action.PasteFrom("Clipboard"),
    },
}

return config
