# udisks

- mount device

udisksctl mount -b /dev/sdxn

- unmount device

udisksctl unmount -b /dev/sdxn

# trash-cli

trash-put           trash files and directories.

trash-empty         empty the trashcan(s).

trash-list          list trashed files.

trash-restore       restore a trashed file.

trash-rm            remove individual files from the trashcan.

# bluetooth

- rfkill caveat

rfkill list

rfkill (unblock|block) bluetooth 

- start the bluetoothctl interactive command

help

list

agent on

power on

scan on

pair *mac*

connect *mac*

# cups

- local web interface

http://localhost:631/

- list devices

lpinfo -v

- list models

lpinfo -m

- add new queue

lpadmin -p *queue_name* -E -v "*uri*" -m *model*

- set the default printer

lpoptions -d *queue_name*

- activate a printer

cupsenable *queue_name*

- set the printer to accept jobs

cupsaccept *queue_name*


- remove a printer

  - reject incoming entries

  cupsreject *queue_name*

  - disable it

  cupsdisable *queue_name*

  - remove it

  lpadmin -x *queue_name*

- print file

lpr *file*

- check the queue

lpq

- clear the queue

lprm

# sane

- list devices

scanimage -L

- scan image

scanimage --device "*device*" [--resolution 600dpi] --format=png --output-file test.png --progress

- get all available opitons for a specific scanner

scanimage --help --device-name "*device*"

# BLE mouse

echo "blacklist hidp" | sudo tee /etc/modprobe.d/blacklist-hidp.conf
echo -e  "# Fix BLE mouse issue\nuhid" | sudo tee /etc/modules-load.d/uhid.conf
cp --update=none /etc/bluetooth/input.conf{,backup}
sudo sed -ie "s:.*UserspaceHID=.*:UserspaceHID=true:" /etc/bluetooth/input.conf
